import { Route, Switch } from "react-router-dom";
import AllMeetupsPage from "./pages/AllMeetups";
import NewMeetupPage from "./pages/NewMeetup";
import FavouritesPage from "./pages/Favourites";
import Layout from "./components/layout/Layout";

function App() {
  return (
    //path is what comes after your domain e.g. after localhost:3000 or test.com/......
    <Layout>
      
      <Switch>
        <Route path='/' exact>
          <AllMeetupsPage />
        </Route>

        <Route path='/new-meetup'>
          <NewMeetupPage />
        </Route>

        <Route path='/favourites'>
          <FavouritesPage />
        </Route>
      </Switch>
    </Layout>
    
  );
}

export default App;
